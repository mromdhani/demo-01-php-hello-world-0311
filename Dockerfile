FROM image-registry.openshift-image-registry.svc:5000/sandboxmohamed-registry/php-nginx
# Docker image with Nginx 1.20 & PHP-FPM 8.0 on Alpine Linux, Only 40 Mo.

COPY src/ /var/www/html

EXPOSE 8080